class Idea < ApplicationRecord
  validates :title, presence: true
end
