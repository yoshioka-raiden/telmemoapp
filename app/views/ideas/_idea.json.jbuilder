json.extract! idea, :id, :title, :details, :created_nm, :updated_nm, :created_at, :updated_at
json.url idea_url(idea, format: :json)
