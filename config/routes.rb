Rails.application.routes.draw do
  get 'users/index'

  get 'users/show'

  get 'home/index'

  get 'home/authentication'

  devise_for :users
  get 'pages/info'
  get 'test/info'
  resources :users, :only => [:index, :show]

  # root to: redirect('/ideas')
  root to: "ideas#index"
  resources :ideas
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
