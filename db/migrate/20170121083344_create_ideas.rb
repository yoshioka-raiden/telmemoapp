class CreateIdeas < ActiveRecord::Migration[5.0]
  def change
    create_table :ideas do |t|
      t.string :title
      t.text :details
      t.string :created_nm
      t.string :updated_nm
      t.string :status

      t.timestamps
    end
  end
end
